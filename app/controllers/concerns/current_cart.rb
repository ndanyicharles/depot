module CurrentCart
  #the purpose of this module is to set the cart in the session
  private 
  #this method uses the try catch in ruby.
  def set_cart
    @cart = Cart.find(session[:cart_id])
  rescue ActiveRecord::RecordNotFound
    @cart = Cart.create
    session[:cart_id] = @cart.id
  end
end
