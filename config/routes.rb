Rails.application.routes.draw do
  get 'admin' => 'admin#index'

  controller :sessions do 
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end
  resources :users
  resources :orders
  root 'store#index', as: 'store_index'
  get 'store/index'
  resources :products, :line_items, :carts
  resources :products do
    get :who_bought, on: :member
  end
  #resources :line_item ,
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
