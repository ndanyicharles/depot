require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  fixtures :products
  #to ensure all the fields are present
  test "products attributes must not be empty" do
    product = Product.new
    assert product.invalid?
    assert product.errors[:title].any?
    assert product.errors[:description].any?
    assert product.errors[:price].any?
    assert product.errors[:image_url].any?
  end
  #ensure that there are no negative prices
  test "product price must be positive" do
    product = Product.new(title: "My Book Title", description: "yyy", image_url: "zzz.jpg")
    product.price = -1
    assert product.invalid?
    assert_equal ["must be greater than or equal to 0.01"], product.errors[:price]
    product.price = 1
    assert product.valid?
  end 
  #ensure the correct image extensions are used
  def new_product(image_url)
    Product.new(title: "My Book Title", description: "yyy", price: 1,image_url: image_url)
  end
  test "image url" do
    ok = %w{fred.gif fred.jpg fred.png FRED.JPG FRED.Jpg http://a.b.c/x/y/z/fred.gif }
    bad = %w{ fred.doc fred.gif/more fred.gif.more }

    #for each name in the array check to confirm if it asserts to being true 
    ok.each do |name|
      #assert that the correct imange names pass as ok
      assert new_product(name).valid?, "#{name} shouldn't be invalid"
    end 
    bad.each do |name|
      assert new_product(name).invalid?, "#{name} shouldn't be valid"
    end
  end 
  test "product not valid without unique title" do
    product = Product.new(title: products(:ruby).title, description: "yyy", price: 1, image_url: "fred.gif")
    #the product should produce false because we are repeating the title    
    assert product.invalid?
    #the test should produce the error as being the titile has already been taken
    assert_equal ["has already been taken"], product.errors[:title]
  end
  #same test as above, when comparing the error message against the built in error message table
  test "products not valid without unique title =i18n" do
    product = Product.new(title: products(:ruby).title, description: "yyy", price: 1, image_url: "fred.gif") 
    assert product.invalid? 
    assert_equal [I18n.translate('errors.messages.taken')], product.errors[:title]
  end
end






















